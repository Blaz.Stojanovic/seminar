epsi = importdata("eps_data.txt");

d10 = importdata("ezdata10.txt");
d20 = importdata("ezdata20.txt");
d30 = importdata("ezdata30.txt");

x = linspace(-20.625, 20.625, 1500);
y = linspace(-13.75, 13.75, 1000);

[X,Y] = meshgrid(x,y); 
eps = 0.5;

f1 = setfig('b1', [800, 400])
h1 = pcolor(X, Y, d10')
text(15, 10, '$t = 10$')
colormap(redblue)
set(h1, 'EdgeColor', 'none')
cb = colorbar
cb.Label.Interpreter = 'latex';
cb.XLabel.String = "${E}_z$";
cb.FontSize  = 20;
caxis([-0.7 0.7])
xlim([min(x) - eps, max(x)+eps])
ylim([min(y)-eps, max(y)+eps])
xlabel('$x [\mu m]$')
ylabel('$y [\mu m]$')
exportf(f1, 'cellt10.png')

f2 = setfig('b2', [800, 400])
h2 = pcolor(X, Y, d20')
text(15, 10, '$t = 20$')
colormap(redblue)
set(h2, 'EdgeColor', 'none')
cb = colorbar
cb.Label.Interpreter = 'latex';
cb.XLabel.String = "${E}_z$";
cb.FontSize  = 18;
caxis([-0.7 0.7])
xlim([min(x) - eps, max(x)+eps])
ylim([min(y)-eps, max(y)+eps])
xlabel('$x [\mu m]$')
ylabel('$y [\mu m]$')
exportf(f2, 'cellt20.png')

f3 = setfig('b3', [800, 400])
h3 = pcolor(X, Y, d30')
text(15, 10, '$t = 30$')
colormap(redblue)
set(h3, 'EdgeColor', 'none')
cb = colorbar
cb.Label.Interpreter = 'latex';
cb.XLabel.String = "${E}_z$";
cb.FontSize  = 18;
caxis([-0.7 0.7])
xlim([min(x) - eps, max(x)+eps])
ylim([min(y)-eps, max(y)+eps])
xlabel('$x [\mu m]$')
ylabel('$y [\mu m]$')
exportf(f3, 'cellt30.png')

f4 = setfig('b4', [800, 400])
h4 = pcolor(X, Y, epsi'.^10)
colormap(flipud(gray))
set(h4, 'EdgeColor', 'none')
% colorbar
% xlim([min(x)/2 - 2*eps/2, max(x)/2+2*eps/2])
% ylim([min(y)/2 - 2*eps/2, max(y)/2+2*eps/2])
xlim([min(x) - eps, max(x)+eps])
ylim([min(y)-eps, max(y)+eps])
xlabel('$x [\mu m]$')
ylabel('$y [\mu m]$')
exportf(f4, 'cell.png')

% h2 = pcolor(X, Y, eps')
% colormap(flipud(gray))
% set(h2, 'EdgeColor', 'none');
