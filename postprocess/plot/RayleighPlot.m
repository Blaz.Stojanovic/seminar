%set parameters, PAZI POVSOD SO NANOMETRI, ZAENKRAT SE LEPO POKRAJSA
lambda = [380, 420, 460, 500, 540, 580, 620, 660, 740, 780];

%colors
% c1 = [97 0 97]/255;
c2 = [106 0 255]/255;
c3 = [0 123 255]/255;
c4 = [0 255 146]/255;
c5 = [129 255 0]/255;
c6 = [255 255 0]/255;
c7 = [255 119 0]/255;
c8 = [255 33 0]/255;
c9 = [255 0 0]/255;
c10 = [97 0 0]/255;
col = [c2; c3; c4; c5; c6; c7; c8; c9; c10];
%physical parameters
theta = linspace(0, 2*pi, 1000);
a = 10; %10 nm
n = 1.33;
r = 10; %oddaljenost
rayleigh = @(th, lam) 8*pi^4*a^6/lam^4/r^2*(n^2 - 1)^2/(n^2 + 2)^2*(1+(cos(th)).^2);
for l = linspace(1,9,9)
    polarplot(theta, rayleigh(theta, lambda(l)), 'color', col(l, :));
    hold on
end
% set(0, 'DefaultTextInterpreter','latex')
% set(0, 'DefaultAxesTickLabelInterpreter', 'latex')
% set(0, 'DefaultColorbarTickLabelInterpreter', 'latex')
% set(0, 'DefaultLegendInterpreter', 'latex')
% 
% set(0, 'DefaultTextFontSize', 20); %default 12
% set(0, 'DefaultAxesFontSize', 20);  %default 12
% set(0, 'DefaultLineLineWidth', 1.2); %default 0.7
% set(0, 'DefaultLineMarkerSize', 10); %default 8
% exportf(gca, 'RayleighPolarMatl.png')
