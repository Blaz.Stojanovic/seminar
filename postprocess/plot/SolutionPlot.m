close all

load('data/r1.mat')
load('data/r2.mat')
load('data/insol.mat')
load('data/outsol.mat')

% absolute value plots

f1 = setfig('a1', [840 840]);
scatter(r1(:, 1), r1(:, 2), 50, abs(insol),  'filled')
xlabel('$x$')
ylabel('$y$')
xlim([-0.3 0.3])
ylim([-0.3 0.3])
title("Vrednosti $|v|$  v cilindru")
axis equal
cb = colorbar
cb.Label.Interpreter = 'latex';
cb.XLabel.String = "$|v|$";
cb.FontSize  = 20;
exportf(f1, 'InsideField.png')

f2 = setfig('a2', [840 840]);
scatter(r2(:, 1), r2(:, 2), 10, abs(outsol),  'filled')
title("Vrednosti $|u^s|$  zunaj cilindra")
xlabel('$x$')
ylabel('$y$')
xlim([-0.8 0.8])
ylim([-0.8 0.8])
axis equal
cb = colorbar
cb.Label.Interpreter = 'latex';
cb.XLabel.String = "$|u|^s$";
cb.FontSize  = 20;
exportf(f2, 'OutsideField.png')

% f1 = setfig('b1', [840 840]);
% scatter(r1(:, 1), r1(:, 2), 'k.')
% scatter(r2(:, 1), r2(:, 2), 'k.')
% xlabel('$x$')
% ylabel('$y$')
% ylim([0.6 0.8])
% xlim([0.1 0.3])
% title("RBF-FD diskretizacija")
% exportf(f1, 'RBFFDPoints.png')
% 
% N = sqrt(length(r1) + length(r2))
% x = linspace(-0.75, 0.75, N);
% y = linspace(-0.75, 0.75, N);
% [X, Y] = meshgrid(x, y);
% X = X(:);
% Y = Y(:);
% R = X.^2 + Y.^2;
% id = R <= 0.75^2;
% 
% f2 = setfig('b2', [840 840]);
% scatter(X(id), Y(id), 'k.')
% title("FDM diskretizacija")
% xlabel('$x$')
% ylabel('$y$')
% ylim([0.6 0.8])
% xlim([0.1 0.3])
% exportf(f2, 'FDMPoints.png')
