import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

rc('text', usetex=True)

d10 = np.loadtxt('ezdata10.txt')
eps = np.loadtxt('eps_data.txt')

x = np.linspace(-20.625, 20.625, 1500);
y = np.linspace(-13.75, 13.75, 1000);

X, Y = np.meshgrid(x, y)


plt.figure()
# plt.pcolor(X, Y, d10.transpose(), cmap='RdBu', alpha=0.6)
plt.scatter(X.flatten(), Y.flatten(), c=(eps.transpose()).flatten(), cmap='binary')
plt.colorbar()
# plt.axis('off')
plt.show()